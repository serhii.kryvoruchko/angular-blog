import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { map } from "rxjs/operators";
import { environment } from "src/environments/environment";
import { FbCreareResponse, Post } from "./interfaces";

@Injectable({ providedIn: 'root' })
export class PostsService {
   constructor(private http: HttpClient) {

   }
   create(post: Post): Observable<Post> {
      return this.http.post(`${environment.fbDburl}/posts.json`, post)
         .pipe(
            map((response: FbCreareResponse) => {
               console.log(response);

               return {
                  ...post,
                  id: response.name,
                  date: new Date(post.date)
               }
            })
         )
   }
   getAll(): Observable<Post[]> {
      return this.http.get(`${environment.fbDburl}/posts.json`)
         .pipe(map((response: { [key: string]: any }) => {
            console.log('A nyka 4e tyt:', response);

            return Object
               .keys(response)
               .map(key => ({
                  ...response[key],
                  id: key,
                  date: new Date(response[key].date)
               }))
         }))
   }

   getById(id: string): Observable<Post> {
      return this.http.get<Post>(`${environment.fbDburl}/posts/${id}.json`)
         .pipe(
            map((post: Post) => {
               return {
                  ...post,
                  id,
                  date: new Date(post.date)
               }
            })
         )
   }

   remove(id: string): Observable<void> {
      return this.http.delete<void>(`${environment.fbDburl}/posts/${id}.json`);
   }

   update(post: Post): Observable<Post> {
      return this.http.patch<Post>(`${environment.fbDburl}/posts/${post.id}.json`, post);
   }
}