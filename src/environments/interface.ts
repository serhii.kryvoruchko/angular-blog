export interface Environment {
   production: boolean
   apiKey: string,
   fbDburl: string
}